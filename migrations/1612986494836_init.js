/* eslint-disable camelcase */
require('node-pg-migrate');
exports.shorthands = undefined;

/**
 * @param {import('node-pg-migrate')} pgm
 */
exports.up = pgm => {
    pgm.createTable('jugements', {
        id: 'id',
        account_id: { type: 'varchar(50)', notNull: true },
        type: { type: 'int', notNull: true, default: 0 },
        discord_verified: { type: 'boolean', notNull: true, default: false },
        email_verified: { type: 'boolean', notNull: true, default: false },
        raw_data: {
            type: 'bytea',
            notNull: true,
        },
    });

    pgm.addConstraint('jugements', 'FC_jugementsName', { unique: ['account_id'] })

    pgm.createTable('discord_data', {
        id: 'id',
        account_id: { type: 'varchar(50)', notNull: true },
        discord_id: { type: 'varchar(32)', notNull: true },
        code: { type: 'varchar(32)', notNull: true },
    });
    pgm.addConstraint('discord_data', 'FC_discord_data', { unique: ['account_id', 'discord_id'] })

    pgm.createTable('mail_data', {
        id: 'id',
        account_id: { type: 'varchar(50)', notNull: true },
        mail: { type: 'varchar(255)', notNull: true },
        code: { type: 'varchar(32)', notNull: true },
    });
    pgm.addConstraint('mail_data', 'FC_mail_data', { unique: ['account_id', 'mail'] })
};

exports.down = pgm => { };
