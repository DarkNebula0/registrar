import { createTransport, SentMessageInfo } from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';

export default class MailClient {
  private static _instance: MailClient;
  private transport: Mail;

  public constructor() {
    this.transport = null;
  }

  public static Initialize() {
    if (!this.Instance.transport) {
      this.Instance.transport = createTransport({
        host: process.env.MAIL_SERVER,
        port: +process.env.MAIL_PORT_OUT,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASSWORD,
        },
      });
    }
  }

  public async sendMail(
    mailAddress: string,
    subject: string,
    msg: string,
  ): Promise<SentMessageInfo> {
    return await this.transport.sendMail({
      from: process.env.MAIL_NAME,
      to: mailAddress,
      subject,
      text: msg,
    });
  }

  public static get Instance() {
    return this._instance || (this._instance = new MailClient());
  }
}
