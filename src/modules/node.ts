import { ApiPromise, WsProvider } from '@polkadot/api';
import HandleChainEvents from '../handler/chainEventHandler';

export default class Node {
  private static _instance: Node;
  private api: ApiPromise = null;
  private provider: WsProvider = null;

  public constructor() {
    this.provider = new WsProvider(process.env.NODE_URL);
  }

  private onDisconected(): void {
    console.log('Reconnection to api');
    this.connect();
  }

  private registerEvents(): void {
    this.api.query.system.events(HandleChainEvents);
  }

  public getApi(): ApiPromise {
    return this.api;
  }

  public async connect(): Promise<Boolean> {
    if (!this.api) {
      this.api = await ApiPromise.create({
        provider: this.provider,
        types: {
          Address: 'IndicesLookupSource',
          LookupSource: 'IndicesLookupSource',
          Nonce: 'u64',
          EventMessage: 'Vec<u8>',
          Campaign: {
            id: 'Hash',
            manager: 'AccountId',
            deposit: 'Balance',
            expiry: 'BlockNumber',
            cap: 'Balance',
            name: 'Vec<u8>',
            protocol: 'u8',
            status: 'u8',
          },
          Treasury: {
            id: 'Hash',
            campaign_id: 'Hash',
            purpose: 'Vec<u8>',
            amount: 'Balance',
            expiry: 'BlockNumber',
            status: 'u8',
          },
        },
      });
    } else {
      await this.api.connect();
    }

    if (this.api.isConnected) {
      // Register events
      this.registerEvents();
    }

    this.api.on('disconnected', this.onDisconected);
    return this.api.isConnected;
  }

  public static get Instance() {
    return this._instance || (this._instance = new Node());
  }
}
