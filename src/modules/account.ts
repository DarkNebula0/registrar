import { Keyring } from '@polkadot/api';
import { AddressOrPair } from '@polkadot/api/types';

export default class Account {
  private static _instance: Account;
  private keyRing: Keyring;
  private keyPair: any;

  constructor() {
    this.keyRing = new Keyring({ type: 'sr25519' });
    this.keyPair = this.keyRing.addFromMnemonic(process.env.MNEMONIC);
  }

  public getKeyRing(): Keyring {
    return this.keyRing;
  }

  public getKeyPair(): AddressOrPair {
    return this.keyPair;
  }

  public static get Instance() {
    return this._instance || (this._instance = new Account());
  }
}
