import to from 'await-to-js';
import { Client, Message, MessageEmbed } from 'discord.js';
import HandleDiscordMessaget from '../events/handleDiscordMessage';

export default class DiscordClient {
  private static _instance: DiscordClient;
  private client: Client;

  public constructor() {
    this.client = new Client();
  }

  private onReady() {
    console.log(`Logged in as ${this.client.user.tag}!`);
  }

  private onMessage(msg: Message) {
    HandleDiscordMessaget(msg);
  }

  public async connect(): Promise<boolean> {
    const [err] = await to(this.client.login(process.env.TOKEN));

    if (!err) {
      this.client.on('ready', this.onReady.bind(this));
      this.client.on('message', this.onMessage.bind(this));
      return true;
    }

    console.log('[DiscordClient]', err);
    return false;
  }

  public createHelpEmbed(): MessageEmbed {
    return (
      new MessageEmbed()
        .setColor('#0099ff')
        .setTitle('I hope that I can help you with this...')
        .addField('Show help', '```!vt help```')
        .addField('---', '_ _')
        //TODO ADD
        .addField('---', '_ _')
        .setTimestamp()
        .setImage(this.client.user.avatarURL())
        .setFooter(this.client.user.username)
    );
  }

  public async sendMessageToUser(
    userId: string,
    msg: string,
  ): Promise<boolean> {
    const user = await this.client.users.fetch(userId);

    if (user) {
      await user.send(msg);
      return true;
    }

    return false;
  }

  public static get Instance() {
    return this._instance || (this._instance = new DiscordClient());
  }
}
