import { config as ImportConfig } from 'dotenv';
import RunMigration from './db/migration';
import Node from './modules/node';
import express from 'express';
import judgmentRouter from './routers/judgementStateRouter';
import DiscordClient from './modules/discordClient';
import discordVerifyRouter from './routers/discordVerifyRouter';
import MailClient from './modules/mailClient';
import mailVerifyRouter from './routers/mailVerifyRouter';
const bodyParser = require('body-parser');
const cors = require('cors');

ImportConfig();

if (!process.env.NODE_URL) {
  console.error(`The environment variable 'NODE_URL' was not set.`);
  process.exit();
}

if (!process.env.REGISTRAR_INDEX) {
  console.error(`The environment variable 'REGISTRAR_INDEX' was not set.`);
  process.exit();
}

if (!process.env.TOKEN) {
  console.error(`The environment variable 'TOKEN' was not set `);
  process.exit();
}

if (!process.env.MAIL_PASSWORD) {
  console.error(`The environment variable 'MAIL_PASSWORD' was not set `);
  process.exit();
}

if (!process.env.MAIL_USER) {
  console.error(`The environment variable 'MAIL_USER' was not set `);
  process.exit();
}

if (!process.env.MAIL_SERVER) {
  console.error(`The environment variable 'MAIL_SERVER' was not set `);
  process.exit();
}

if (!process.env.MAIL_PORT_OUT) {
  console.error(`The environment variable 'MAIL_PORT_OUT' was not set `);
  process.exit();
}

if (!process.env.MAIL_NAME) {
  console.error(`The environment variable 'MAIL_NAME' was not set `);
  process.exit();
}

if (!process.env.FORMATS) {
  console.error(`The environment variable 'FORMATS' was not set `);
  process.exit();
}

console.log(`Registrar for index: ${process.env.REGISTRAR_INDEX}`);

const node = Node.Instance;
const discord = DiscordClient.Instance;
const mail = MailClient.Instance;
MailClient.Initialize();

(async () => {
  await RunMigration();
  const isNodeConnected = await node.connect();
  const isDiscordConnected = await discord.connect();
  console.log(
    'isNodeConnected',
    isNodeConnected,
    ' isDiscordConnected',
    isDiscordConnected,
  );
})();

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((_, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  next();
});

app.use('/judgement', judgmentRouter);
app.use('/verify-discord', discordVerifyRouter);
app.use('/verify-mail', mailVerifyRouter);

const port = 8080;
app.listen(port, () =>
  console.log(`Provider Service listening on port ${port}!`),
);
