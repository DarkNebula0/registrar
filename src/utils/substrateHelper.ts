import { checkAddress } from '@polkadot/util-crypto';
const { BN } = require('bn.js');

export default class SubstrateHelper {
  public static toUnit(balance: String, decimals: String) {
    const base = new BN(10).pow(new BN(decimals));
    const dm = new BN(balance).divmod(base);
    return parseFloat(dm.div.toString() + '.' + dm.mod.toString());
  }

  public static checkAddressFormats(address: String) {
    let isValid = false;
    process.env.FORMATS.split(',')
      .map((x) => +x)
      .forEach((prefix) => {
        if (checkAddress(address.toString(), prefix)[0]) {
          isValid = true;
        }
      });

    if (isValid) {
      return false;
    }

    return new Error(
      'The address you entered is wrong. Please use one of the supported formats [Zero, Substrate, Polkadot Relay Chain, Kusama Relay Chain]',
    );
  }
}
