import { Request, Response } from 'express';
import MailDataTable from '../db/tables/emailData';
import JudgementsTable from '../db/tables/judgementsTable';
import MailClient from '../modules/mailClient';
import Node from '../modules/node';

export async function post_mailCode(req: Request, res: Response) {
  const { account_id, code } = req.body || {};

  if (!account_id || !code) {
    return res.sendStatus(400);
  }

  try {
    const judgement = await JudgementsTable.getJudgementForAccountId(
      account_id,
    );
    const mailData = await MailDataTable.getDataForAccountId(account_id);

    if (!judgement || !mailData) {
      return res.sendStatus(404);
    }

    const api = Node.Instance.getApi();

    const identity = await api.query.identity.identityOf(account_id);

    if (!identity || !identity.value) {
      return res.sendStatus(404);
    }

    const buffer = Buffer.from(
      JSON.stringify((identity.value as any).info || ''),
    );

    if (Buffer.compare(buffer, judgement.raw_data) !== 0) {
      await JudgementsTable.removeJudgement(judgement.id, judgement.account_id);
      return res
        .status(406)
        .send(
          'The identity has changed since the register. The order will be deleted.',
        );
    }

    if (judgement.email_verified) {
      return res.status(406).send('The account is already verified.');
    }

    let emailAddress = Buffer.from(
      (identity.value as any).info.email?.asRaw || '',
    )
      .toString()
      .toLowerCase();

    if (emailAddress !== mailData.mail) {
      return res.status(401).send('Account mismatch.');
    }

    if (mailData.code !== code) {
      return res.status(401).send('The code does not match try another one.');
    }

    const updateState = JudgementsTable.updateMailState(
      judgement.id,
      judgement.account_id,
      true,
    );
    if (!updateState) {
      throw new Error('An error has occurred try again in a few minutes.');
    }
  } catch (err) {
    console.error('[post_mailCode]', err);
    return res.status(500).send(err.message);
  }

  return res.sendStatus(200);
}

export async function post_getVerifyCode(req: Request, res: Response) {
  const { account_id } = req.body || {};

  if (!account_id) {
    return res.sendStatus(400);
  }

  try {
    const judgement = await JudgementsTable.getJudgementForAccountId(
      account_id,
    );

    if (!judgement) {
      return res.sendStatus(404);
    }

    const api = Node.Instance.getApi();

    const identity = await api.query.identity.identityOf(account_id);

    if (!identity || !identity.value) {
      return res.sendStatus(404);
    }

    const buffer = Buffer.from(
      JSON.stringify((identity.value as any).info || ''),
    );

    if (Buffer.compare(buffer, judgement.raw_data) !== 0) {
      await JudgementsTable.removeJudgement(judgement.id, judgement.account_id);
      return res
        .status(406)
        .send(
          'The identity has changed since the register. The order will be deleted.',
        );
    }

    if (judgement.email_verified) {
      return res.status(406).send('The account is already verified.');
    }

    let emailAddress = Buffer.from(
      (identity.value as any).info.email?.asRaw || '',
    )
      .toString()
      .toLowerCase();

    if (!emailAddress) {
      return res
        .status(404)
        .send('The identity data are incorrect and need to be updated.');
    }

    let data = await MailDataTable.getDataForMailAddress(emailAddress);
    if (data) {
      await MailDataTable.removeData(data.account_id, emailAddress);
    }

    const valid = await MailDataTable.insertData(account_id, emailAddress);
    if (!valid) {
      throw new Error('The entry could not be written to the database ');
    }

    data = await MailDataTable.getDataForMailAddress(emailAddress);

    await MailClient.Instance.sendMail(
      emailAddress,
      `Code for ${data.account_id}`,
      `Your code for the validation is: ${data.code}`,
    );
  } catch (err) {
    console.error('[post_getVerifyCode]', err);
    return res.status(500).send(err.message);
  }

  return res.sendStatus(200);
}
