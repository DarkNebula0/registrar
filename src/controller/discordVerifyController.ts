import { Request, Response } from 'express';
import DiscordDataTable from '../db/tables/discordData';
import JudgementsTable from '../db/tables/judgementsTable';
import DiscordClient from '../modules/discordClient';
import Node from '../modules/node';

export async function post_discordCode(req: Request, res: Response) {
  const { account_id, code } = req.body || {};

  if (!account_id || !code) {
    return res.sendStatus(400);
  }

  try {
    const judgement = await JudgementsTable.getJudgementForAccountId(
      account_id,
    );
    const discordData = await DiscordDataTable.getDataForAccountId(account_id);

    if (!judgement || !discordData) {
      return res.sendStatus(404);
    }

    const api = Node.Instance.getApi();

    const identity = await api.query.identity.identityOf(account_id);

    if (!identity || !identity.value) {
      return res.sendStatus(404);
    }

    const buffer = Buffer.from(
      JSON.stringify((identity.value as any).info || ''),
    );

    if (Buffer.compare(buffer, judgement.raw_data) !== 0) {
      await JudgementsTable.removeJudgement(judgement.id, judgement.account_id);
      return res
        .status(406)
        .send(
          'The identity has changed since the register. The order will be deleted.',
        );
    }

    if (judgement.discord_verified) {
      return res.status(406).send('The account is already verified.');
    }

    let hasValidId = false;
    (identity.value as any).info.additional?.forEach((items: any) => {
      const field = Buffer.from(items?.[0]?.asRaw || '')
        .toString()
        .toLowerCase();
      if (field === 'discord') {
        if (
          discordData.discord_id.toLowerCase() ===
          Buffer.from(items?.[1]?.asRaw || '')
            .toString()
            .toLowerCase()
        ) {
          hasValidId = true;
        }
      }
    });

    if (!hasValidId) {
      return res.status(401).send('Account mismatch.');
    }

    if (discordData.code !== code) {
      return res.status(401).send('The code does not match try another one.');
    }

    const updateState = JudgementsTable.updateDiscordState(
      judgement.id,
      judgement.account_id,
      true,
    );
    if (!updateState) {
      throw new Error('An error has occurred try again in a few minutes.');
    }
  } catch (err) {
    console.error('[post_discordCode]', err);
    return res.status(500).send(err.message);
  }

  return res.sendStatus(200);
}

export async function post_getVerifyCode(req: Request, res: Response) {
  const { account_id } = req.body || {};

  if (!account_id) {
    return res.sendStatus(400);
  }

  try {
    const judgement = await JudgementsTable.getJudgementForAccountId(
      account_id,
    );

    if (!judgement) {
      return res.sendStatus(404);
    }

    const api = Node.Instance.getApi();

    const identity = await api.query.identity.identityOf(account_id);

    if (!identity || !identity.value) {
      return res.sendStatus(404);
    }

    const buffer = Buffer.from(
      JSON.stringify((identity.value as any).info || ''),
    );

    if (Buffer.compare(buffer, judgement.raw_data) !== 0) {
      await JudgementsTable.removeJudgement(judgement.id, judgement.account_id);
      return res
        .status(406)
        .send(
          'The identity has changed since the register. The order will be deleted.',
        );
    }

    if (judgement.discord_verified) {
      return res.status(406).send('The account is already verified.');
    }

    let discordId = null;
    (identity.value as any).info.additional?.forEach((items: any) => {
      const field = Buffer.from(items?.[0]?.asRaw || '')
        .toString()
        .toLowerCase();
      if (field === 'discord') {
        discordId = Buffer.from(items?.[1]?.asRaw || '')
          .toString()
          .toLowerCase();
      }
    });

    if (!discordId) {
      return res
        .status(404)
        .send('The identity data are incorrect and need to be updated.');
    }

    let data = await DiscordDataTable.getDataForDiscordId(discordId);
    if (data) {
      await DiscordDataTable.removeData(data.account_id, data.discord_id);
    }

    const valid = await DiscordDataTable.insertData(account_id, discordId);
    if (!valid) {
      throw new Error('The entry could not be written to the database ');
    }

    data = await DiscordDataTable.getDataForDiscordId(discordId);

    DiscordClient.Instance.sendMessageToUser(
      discordId,
      `Your code for: ${data.account_id} is: \`\`\`${data.code}\`\`\``,
    );
  } catch (err) {
    console.error('[post_getVerifyCode]', err);
    return res.status(500).send(err.message);
  }

  return res.sendStatus(200);
}
