import { Request, Response } from 'express';
import JudgementsTable from '../db/tables/judgementsTable';

export async function get_judgementState(req: Request, res: Response) {
  if (!req.params || !req.params.accountId) {
    return res.sendStatus(400);
  }

  const jugement = await JudgementsTable.getJudgementForAccountId(
    req.params.accountId,
  );

  if (!jugement) {
    return res.sendStatus(404);
  }

  res.status(200).send(
    JSON.stringify({
      type: jugement.type,
      discord_verified: jugement.discord_verified,
      email_verified: jugement.email_verified,
    }),
  );
}
