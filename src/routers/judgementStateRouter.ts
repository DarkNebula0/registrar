import express from 'express';
import { get_judgementState } from '../controller/judgementStateController';

const judgmentRouter = express.Router();
judgmentRouter.get('/:accountId', get_judgementState);

export default judgmentRouter;
