import express from 'express';
import {
  post_discordCode,
  post_getVerifyCode,
} from '../controller/discordVerifyController';

const discordVerifyRouter = express.Router();
discordVerifyRouter.post('/', post_discordCode);
discordVerifyRouter.post('/code', post_getVerifyCode);

export default discordVerifyRouter;
