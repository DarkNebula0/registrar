import express from 'express';
import {
  post_getVerifyCode,
  post_mailCode,
} from '../controller/mailVerifyController';

const mailVerifyRouter = express.Router();
mailVerifyRouter.post('/', post_mailCode);
mailVerifyRouter.post('/code', post_getVerifyCode);

export default mailVerifyRouter;
