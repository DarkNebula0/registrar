const { default: migrate } = require('node-pg-migrate');

export default async function RunMigration() {
  const { DB_USER, DB_PASSWORD, DB_DATABASE, DB_HOST, DB_PORT } = process.env;
  const options = {
    databaseUrl: {
      host: DB_HOST,
      port: DB_PORT,
      user: DB_USER,
      password: DB_PASSWORD,
      database: DB_DATABASE,
    },
    migrationsTable: 'pgmigrations',
    dir: 'migrations',
    direction: 'up',
    count: Infinity,
  };

  try {
    await migrate(options);
  } catch (e) {
    console.error('DB migration has failed', e);
    process.exit(1);
  }
}
