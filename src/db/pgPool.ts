import { Pool, QueryResult } from 'pg';

export default class DB {
  private static _instance: DB;
  private pool: Pool;

  constructor() {
    const { DB_USER, DB_PASSWORD, DB_DATABASE, DB_HOST, DB_PORT } = process.env;
    this.pool = new Pool({
      host: DB_HOST,
      port: +DB_PORT,
      user: DB_USER,
      password: DB_PASSWORD,
      database: DB_DATABASE,
    });

    this.pool.on('error', this.onError);
  }

  private onError(err: Error): void {
    console.error(err);
  }

  public query(text: string, params: any[] = null, callback: any = null): any {
    return this.pool.query(text, params, callback);
  }

  public static get Instance() {
    return this._instance || (this._instance = new DB());
  }
}
