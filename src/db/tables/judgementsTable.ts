import { bool } from '@polkadot/types';
import { JudgementStates } from '../../constants/judgementState';
import DB from '../pgPool';

interface JudgementsTableData {
  id: string;
  account_id: string;
  type: number;
  discord_verified: boolean;
  email_verified: bool;
  raw_data: Buffer;
}

export default class JudgementsTable {
  public static async getJudgementForAccountId(
    accountId: string,
  ): Promise<JudgementsTableData> {
    const result = await DB.Instance.query(
      `SELECT * FROM jugements WHERE account_id='${accountId}'`,
    );

    return result ? result.rows[0] : null;
  }

  public static async insertJudgement(
    accountId: string,
    identity: Buffer,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `INSERT INTO jugements (account_id,type,raw_data) VALUES ('${accountId}','${JudgementStates.REQUESTED}','${identity}')`,
    );

    return result.rowCount === 1;
  }

  public static async removeJudgement(
    id: string,
    accountId: string,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `DELETE FROM jugements WHERE id='${id}' and account_id='${accountId}'`,
    );

    return result.rowCount === 1;
  }

  public static async updateType(
    id: string,
    accountId: string,
    state: JudgementStates,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `UPDATE jugements SET type='${state}' WHERE id='${id}' and account_id='${accountId}'`,
    );

    return result.rowCount === 1;
  }

  public static async updateDiscordState(
    id: string,
    accountId: string,
    state: boolean,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `UPDATE jugements SET discord_verified='${state}' WHERE id='${id}' and account_id='${accountId}'`,
    );

    return result.rowCount === 1;
  }

  public static async updateMailState(
    id: string,
    accountId: string,
    state: boolean,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `UPDATE jugements SET email_verified='${state}' WHERE id='${id}' and account_id='${accountId}'`,
    );

    return result.rowCount === 1;
  }
}
