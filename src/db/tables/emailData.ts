import DB from '../pgPool';

interface EmailDataTableData {
  id: String;
  account_id: String;
  mail: String;
  code: String;
}

export default class MailDataTable {
  public static async getDataForAccountId(
    accountId: String,
  ): Promise<EmailDataTableData> {
    const result = await DB.Instance.query(
      `SELECT * FROM mail_data WHERE account_id='${accountId}'`,
    );

    return result ? result.rows[0] : null;
  }

  public static async getDataForMailAddress(
    mail: String,
  ): Promise<EmailDataTableData> {
    const result = await DB.Instance.query(
      `SELECT * FROM mail_data WHERE mail='${mail}'`,
    );

    return result ? result.rows[0] : null;
  }

  public static async insertData(
    accountId: String,
    mail: String,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `INSERT INTO mail_data (account_id,mail,code) VALUES ('${accountId}','${mail}','${`${
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15)
      }`.substr(0, 32)}')`,
    );

    return result.rowCount === 1;
  }

  public static async removeData(
    accountId: String,
    mail: String,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `DELETE FROM mail_data WHERE mail='${mail}' and account_id='${accountId}'`,
    );

    return result.rowCount === 1;
  }
}
