import DB from '../pgPool';

interface DiscordDataTableData {
  id: String;
  account_id: String;
  discord_id: String;
  code: String;
}

export default class DiscordDataTable {
  public static async getDataForAccountId(
    accountId: String,
  ): Promise<DiscordDataTableData> {
    const result = await DB.Instance.query(
      `SELECT * FROM discord_data WHERE account_id='${accountId}'`,
    );

    return result ? result.rows[0] : null;
  }

  public static async getDataForDiscordId(
    discordId: String,
  ): Promise<DiscordDataTableData> {
    const result = await DB.Instance.query(
      `SELECT * FROM discord_data WHERE discord_id='${discordId}'`,
    );

    return result ? result.rows[0] : null;
  }

  public static async insertData(
    accountId: String,
    discordId: String,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `INSERT INTO discord_data (account_id,discord_id,code) VALUES ('${accountId}','${discordId}','${`${
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15)
      }`.substr(0, 32)}')`,
    );

    return result.rowCount === 1;
  }

  public static async removeData(
    accountId: String,
    discordId: String,
  ): Promise<boolean> {
    const result = await DB.Instance.query(
      `DELETE FROM discord_data WHERE discord_id='${discordId}' and account_id='${accountId}'`,
    );

    return result.rowCount === 1;
  }
}
