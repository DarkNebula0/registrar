export enum JudgementStates {
  REQUESTED = 0,
  OPEN,
  CLOSED,
  CANCELED,
}
