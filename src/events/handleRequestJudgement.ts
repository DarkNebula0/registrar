import { EventRecord, IdentityJudgement } from '@polkadot/types/interfaces';
import JudgementsTable from '../db/tables/judgementsTable';
import Account from '../modules/account';
import Node from '../modules/node';

export default async function HandleRequestJudgement(eventRecord: EventRecord) {
  if (!eventRecord) {
    return;
  }

  const { event } = eventRecord;

  if (event.section !== 'identity' || event.method !== 'JudgementRequested') {
    console.warn('Event is invalid and will be skipped', event);
    return;
  }

  if (event.data[1].toString() !== process.env.REGISTRAR_INDEX) {
    // The event is intended for another registrar
    return;
  }

  const accountId = event.data[0].toString();
  if (!accountId) {
    console.warn('AccountId was not specified');
    return;
  }

  const api = Node.Instance.getApi();
  const identity = await api.query.identity.identityOf(accountId);

  if (!identity) {
    console.error('Identity could not be retrieved');
    return;
  }

  const buffer = Buffer.from(
    JSON.stringify((identity.value as any).info || ''),
  );

  const jugement = await JudgementsTable.getJudgementForAccountId(accountId);
  if (jugement) {
    await JudgementsTable.removeJudgement(jugement.id, jugement.account_id);
  }

  await JudgementsTable.insertJudgement(accountId, buffer);

  // api.tx.identity
  //   .provideJudgement(process.env.REGISTRAR_INDEX, accountId, 'KnownGood')
  //   .signAndSend(Account.Instance.getKeyPair());
  // console.log(event);
}
