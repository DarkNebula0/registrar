import { EventRecord, IdentityJudgement } from '@polkadot/types/interfaces';
import { Message } from 'discord.js';
import DiscordClient from '../modules/discordClient';

export default async function HandleDiscordMessaget(msg: Message) {
  if (!msg || msg.author.bot || !msg.content.startsWith('!vt')) {
    return;
  }

  const discordClient = DiscordClient.Instance;

  const msgArgs = msg.content.split(/\s/);
  switch (msgArgs[1]) {
    default:
    case 'help': {
      const embed = discordClient.createHelpEmbed();
      return embed && msg.channel.send(embed);
    }
  }
}
