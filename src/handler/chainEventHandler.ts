import { Vec } from '@polkadot/types';
import { EventRecord } from '@polkadot/types/interfaces';
import HandleRequestJudgement from '../events/handleRequestJudgement';

export default async function HandleChainEvents(events: Vec<EventRecord>) {
  if (Array.isArray(events)) {
    events.forEach((eventRecord: EventRecord) => {
      const {
        event: { section, method },
      } = eventRecord;

      if (section === 'identity') {
        switch (method) {
          case 'JudgementRequested':
            HandleRequestJudgement(eventRecord);
            break;
          default:
            return;
        }
      }
    });
  }
}
